<section id="maincontent">
    <header class="nav mobile-only">Jaunākais lapā</header>
<?php
/*
 * The main page of the community. 
 * Populating 4 articles par page and creating the paginator
 */
if(!isset($_GET['page'])){
    $_GET['page'] = 1;
}
$limit = $_GET['page'];
//Cycle through all articles and add them one by one - 4 per page
$db = new db();
$handler = new dbhandler();
$offset = ($limit-1)*4;

$query="SELECT * FROM articles WHERE hidden = 0 ORDER BY id DESC LIMIT 4 OFFSET $offset"; //"LIMIT 4" = show only 4 | "OFFSET $offset" = starting from offset+1
$result = $db->query($query);

while($row = mysqli_fetch_assoc($result)){
    $heading = $row['heading'];
    $author = $row['author'];
    $date = $row['date'];
    $id = $row['id'];
    $commentcount = $row['commentcount'];
    $preview = substr($row['text'], 0, 400);
    if(strlen($row['text']) > 399){
        $preview  = $preview."...";
    }
    $username = $handler->displayUsername($row['authorid']);
    ?>
        <article>
            <h1 class="title"><a href=<?php echo "article/".$id?>><?php echo $heading; ?></a></h1>
            <h2 class="articledata">No: <?php echo $username." @ ".$date ?> | Komentāri: <?php echo $commentcount ?></h2>
            <p class="articlepreview">
                <?php echo $preview ?>
            </p>
            <a class="readall btn btn-primary btn-xs" href="<?php echo "article/".$id?>">Lasīt visu</a>
        </article>
<?php } ?>

<div class="pagination">
<ul>
<?php
// Good luck to me if I want to read this again (note to future self: you are not a smart man)
// pagination structure
// get amount of only non-deleted articles
$query="SELECT id FROM articles WHERE hidden = 0";
$db->query($query);
$articlecount = $db->affected;
//get number of articles, divide by 4 to get page count
$pages = ceil($articlecount/4);
for($i=1; $i<=$pages; $i++){
?>
<li><a href=<?php echo "page/".$i; ?> <?php echo $_GET['page']==$i ? "class='currentpage'" : false ?>> <?php echo $i ?> </a></li>
<?php } ?>
</ul>
</div>

</section>
