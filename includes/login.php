<?php
    if(isset($_POST['f_username']) && isset($_POST['f_password'])){
        $handler = new dbhandler($_POST['f_username'], $_POST['f_password']);
        $handler->login();
    }
    //are we on register.php? if so, don't display the login form
    if($_GET['viewpage'] !== "register"){
?>
<?php
if(isset($_SESSION['incorrectCredentials'])){
    echo $_SESSION['incorrectCredentials'];
    unset($_SESSION['incorrectCredentials']);
}
if(isset($_SESSION['registrationSuccessful'])){
    echo $_SESSION['registrationSuccessful'];
    unset($_SESSION['registrationSuccessful']);
}
?>
<form role="form" action="#" method="POST">
    <legend>Ielogoties</legend>
    <div class="form-group">
        <label for="niks">Lietotājvārds</label>
        <input name="f_username" type="text" class="form-control" id="niks" placeholder="Lietotājvārds"/>
    </div>
    <div class="form-group">
        <label for="parole">Parole</label>
        <input name="f_password" type="password" class="form-control" id="parole"/>
    </div>
    <input class="btn btn-success" type="submit" value="Ielogoties" />
</form>
<br />
<a class="btn btn-primary" href="register">Neesi reģistrējies?</a>
<?php } ?>