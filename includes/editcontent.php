<?php
    $handler = new dbhandler();
    $commentinfo = $handler->pullCommentByID($_GET['id']);
    //only show the form if the user is logged in AND the user is the author of the post OR the user is NOT a simple user
    if(isset($_SESSION['username']) && ($_SESSION['username'] == $commentinfo['author'] || $_SESSION['usergroup'] > 0)){
        if(isset($_POST['editcomment']) && isset($_POST['editedpost'])){
            $handler->editComment($_GET['id'], $_POST['editedpost']);
        }
?>
<section id="maincontent">
    <form action="" method="post">
        <textarea class="addcommentTextArea" name="editedpost">
            <?php echo $commentinfo['text'] ?>
        </textarea>
        <input type="submit" name="editcomment" class="btn btn-success" value="Labot"/>
    </form>
</section>
<?php }else{
    echo "Tu neesi šī komentāra autors!";
}
?>
