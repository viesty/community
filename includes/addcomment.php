<?php
    if(isset($_SESSION['username'])){
        if(isset($_POST['postcontent'])){    
            $handler = new dbhandler();
            $handler->addComment($_POST['postcontent'], $_GET['id']);
        }
?>

<section id="addcomment">
    <header>Pievienot komentāru</header>
    <?php
    if(isset($_SESSION['incorrectCredentials'])){
        echo $_SESSION['incorrectCredentials'];
        unset($_SESSION['incorrectCredentials']);
    }
    ?>
    <form action="" method="post">
        <textarea class="addcommentTextArea" name="postcontent"></textarea>
        <input type="submit" value="Pievienot!" class="btn btn-primary"/>
    </form>
</section>
<?php }else{?>
<div class="warningBox"><p>Lai komentētu, jābūt <a href="register">reģistrētam</a>!</p></div>
<?php } ?>