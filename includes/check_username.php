<?php
require_once 'autoload.php';
header('Content-type: application/json');
$handler = new dbhandler();

if(isset($_POST['username'])){
    $username = $_POST['username'];
    $handler->getUser($username);
    if($handler->affected === 0){
        $response_array['status'] = 'free';  
    }else{
        $response_array['status'] = 'taken';  
    }
    echo json_encode($response_array);
}
?>