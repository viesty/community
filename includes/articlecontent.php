<section id="maincontent">
<?php
$handler = new dbhandler();

//check for delete parameters
if($_SESSION['usergroup'] > 0 && (isset($_GET['deleteArticle']) || isset($_GET['deleteComment']))){
    if(isset($_GET['deleteArticle'])){
        //hide the article
        $id = $db->sanitize($_GET['deleteArticle']);
        $db->query("UPDATE articles SET hidden=1 WHERE id='$id'");
        header("Location: /community/");
    }
    if(isset($_GET['deleteComment'])){
        $id = $db->sanitize($_GET['deleteComment']);
        //hide the comment
        $db->query("UPDATE comments SET hidden=1 WHERE id='$id'");
        //get article id in which to decrement commentcount
        $decrement = $handler->pullCommentByID($id);
        $decrementID = $decrement['articleid'];
        $decrementUserID = $decrement['authorid'];
        $db->query("UPDATE articles SET commentcount=commentcount-1 WHERE id='$decrementID'");
        $db->query("UPDATE users SET posts=posts-1 WHERE id='$decrementUserID'");
        header("Location: article/".$_SESSION['lastArticle']);
        unset($_SESSION['lastArticle']);
    }
}else{

    
    $result = $handler->pullArticleByID($_GET['id']);
    if($result !== "fail" && $result['hidden'] === '0'){
        $heading = $result['heading'];
        $text = $result['text'];
        $author = $result['author'];
        $date = $result['date'];
        $username = $handler->displayUsername($result['authorid']);
        $edit = ($_SESSION['usergroup'] > 0 || $_SESSION['username'] == $author ? "<a class='editbutton btn btn-success btn-xs' href='post.php?edit=".$_GET['id']."'>Labot</a>" : "");
        $delete = ($_SESSION['usergroup'] > 0 ? "<a class='deletebutton btn btn-warning btn-xs' href='article.php?deleteArticle=".$_GET['id']."'>Dzēst</a>" : "");
?>
    <div id="articlebox">
        <div id="articlecontent">
            <h1><?php echo $heading; echo $edit; echo $delete;?></h1>
            <span class="articletext"><?php echo $text ?></span>
        </div>
        <div id="articlebottom" class="clearfix">
            <?php $handler->displayAvatar($result['authorid']); ?>
            <div id="articleinfo">
                <h2><?php echo $username ?></h2>
                <p>@ <?php echo $date ?></p>
            </div>
        </div>
    </div>
    
    <?php 
    require_once 'comments.php'; 
    require_once 'addcomment.php';
    }else{
        echo "<div class='warningBox'><p>Tāda raksta nav!</p></div>";
    }
    }
    ?>
</section>