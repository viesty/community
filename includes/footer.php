<section class="sidebar">
    <div id="recent">
        <header class="nav">
            Nesenie notikumi
        </header>
        <?php require_once 'recentevents.php'; ?>
    </div>
</section>
<section id="onlineusers">
<p>Lietotāji online: </p>
<?php
    $handler = new dbhandler();
    //get datetime now and 5 minutes before now. 5 minutes of last activity
    //is the period during which a user is considered to be online
    $time = new DateTime(date("Y-m-d H:i:s"));
    $time->sub(new DateInterval("PT5M"));
    $time->format("Y-m-d H:i:s");
    $result = $handler->query("SELECT id FROM users WHERE last_activity>='{$time->format("Y-m-d H:i:s")}'");
    if($handler->affected === 0){
        echo "nav :(";
    }else{
        while($row = mysqli_fetch_assoc($result)){
            echo $handler->displayUsername($row['id']);
        }
    }
?>
</section>

<footer>
    <p class="footer">&copy; <a href="http://ruzans.com">Viesturs Ružāns</a>, <?php echo date("Y"); ?></p>
</footer>
</div>
</body>
</html>