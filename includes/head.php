<?php 
//date to Latvian
switch (date("N")) {
	case 1:
		$day = "Pirmdiena";
		break;
	case 2:
        $day = "Otrdiena";
        break;
	case 3:
        $day = "Trešdiena";
        break;
    case 4:
        $day = "Ceturtdiena";
        break;    
	case 5:
        $day = "Piektdiena";
        break;
	case 6:
        $day = "Sestdiena";
        break;
	case 7:
        $day = "Svētdiena";
        break;
}
switch (date("n")) {
    case 1:
        $month = "janvāris";
        break;
    case 2:
        $month = "februāris";
        break;
    case 3:
        $month = "marts";
        break;
    case 4:
        $month = "aprīlis";
        break;    
    case 5:
        $month = "maijs";
        break;
    case 6:
        $month = "jūnijs";
        break;
    case 7:
        $month = "jūlijs";
        break;
    case 8:
        $month = "augusts";
        break;
    case 9:
        $month = "septembris";
        break;
    case 10:
        $month = "oktobris";
        break;
    case 11:
        $month = "novembris";
        break;
    case 12:
        $month = "decembris";
        break;
}


//if user is logged in, show the sidebar button of messages with an unread number of messages counter.
require_once 'includes/autoload.php';
if(isset($_SESSION['username'])){
    $db = new db();
    //any unread messages for the current user?
    $db->query("SELECT isread FROM messages WHERE recipient='{$_SESSION['userid']}' AND isread=0"); 
    if($db->affected > 0){
        //if yes, then add square brackets and a span of .unreadmessages
        $messages = "<a href='pm'><li><span class='fa fa-at'></span>Vēstules [<span class='unreadmessages'>".$db->affected."</span>]</li></a>";
    }else{
        //otherwise, simply return the default messages title
        $messages = "<a href='pm'><li><span class='fa fa-at'></span>Vēstules</li></a>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="shortcut icon" type="image/x-icon" href="assets/favicon.ico">
        <BASE href="/community/">
        <meta charset="UTF-8">
        <title>Community</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" type="text/css" href="styles/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="styles/main.css" />
        
        <link rel="stylesheet" href="font-awesome-4.2.0/css/font-awesome.min.css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700,400&amp;subset=latin-ext' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="scripts/tinymce/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript" src="scripts/tinymce_textareas.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <header class="mainHeader">
                <a href="/community/">Sākums</a>
                <a href="users">Citi lietotāji</a>
                <a href="stats">Statistika</a>
                <p class="date"><?php echo($day.", ".date("Y").". gada ".date("j").". ".$month); ?></p>
            </header>
            <section class="sidebar">
                <nav>
                    <?php if(isset($_SESSION['username'])){?>
                    <header class="nav">Sveiks, <?php echo $_SESSION['username']; //user greeting ?>!</header>
                    <ul>
                    <a href="profile"><li><span class="fa fa-male"></span>Profils</li></a>
                    <a href="post"><li><span class="fa fa-pencil"></span>Jauns ieraksts</li></a>
                    <?php echo $messages; //echo the messages button of the menu ?>
                    <a href="logout?check=<?php echo $_SESSION['checksum']; ?>"><li><span class="fa fa-power-off"></span>Iziet</li></a>
                    </ul>
                    <?php }else{ require_once 'login.php'; } //if user is not logged in, replace the nav with login?>
                </nav>
            </section>