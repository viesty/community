<?php
    $userinfo = $handler->getUserByID($_SESSION['userid']);
    $homepage = $userinfo['homepage'];
    $skype = $userinfo['skype'];
    $aboutme = $userinfo['aboutme'];
    $maxFileSize = 1000000;
        if(isset($_SESSION['username']) && $_SESSION['userid'] == $_GET['edit'] && isset($_POST['editbutton'])){
            $id = $_GET['edit'];
            $handler->updateProfile($id, strip_tags($_POST['website']), strip_tags($_POST['skype']), $_POST['aboutme']);
        }
?>
<section id="maincontent">
    <header class="contentheader">Profila rediģēšana</header>
    <div id="profiledit">
        <form role="form" action="" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="file_input">Avatars</label>
               <input type="file" name="file_avatar" id="file_input" />
               <p class="help-block">Maksimālais attēla lielums: 1 MB</p>
            </div>
            <div class="form-group">
               <label for="website">Mājaslapa</label>
               <input class="form-control" type="text" name="website" id="website" value="<?php echo $homepage; ?>" />
            </div>
            <div class="form-group">
               <label for="skype">Skype&nbsp;lietotājvārds</label>
               <input class="form-control" type="text" name="skype" id="skype" value="<?php echo $skype; ?>"/>
            </div>
            <div class="form-group">
               <label for="aboutme">Par mani</label>
               <textarea name="aboutme" id="aboutme" class="addarticleTextArea"><?php echo $aboutme ?></textarea>
            </div>
            <input type="submit" value="Saglabāt" name="editbutton" class="btn btn-success"/>
        </form>
    </div>
    
</section>