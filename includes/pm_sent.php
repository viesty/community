<section id="maincontent">
    <nav class="pmnav">
        <ul>
            <a href="pm/received/"><li class="receivedmail">Saņemtās</li></a>
            <a href="pm/sent/"><li class="sentmail pm_activemenu">Sūtītās</li></a>
        </ul>
    </nav>

    <table border="1" cellspacing="25" cellpadding="5" class="messagetable">
        <tr>
            <th></th>
            <th>Temats</th>
            <th>Kam?</th>
            <th>Kad?</th>
        </tr>
        
<?php
    $handler = new dbhandler();
    $result = $handler->query("SELECT * FROM messages WHERE sender='{$_SESSION['userid']}' ORDER BY id DESC");
    while($row = mysqli_fetch_assoc($result)){
        $title = $row['title'];
        $title = "<a href=pm/read/".$row['id'].">".$title."</a>";
        $to = $handler->displayUsername($row['recipient']);
        $date = $row['date'];
        $tr = ($row['isread'] == 0) ? "<tr class='unread'>" : "<tr class='read'>";
?>

        <?php echo $tr ?>
            <td class="image"></td>
            <td class="messagetitle"><?php echo $title ?></td>
            <td><?php echo $to ?></td>
            <td><?php echo $date ?></td>
        </tr>
        
<?php } ?>
    </table>
</section>