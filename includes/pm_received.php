<section id="maincontent">
    <nav class="pmnav">
        <ul>
            <a href="pm/received/"><li class="receivedmail pm_activemenu">Saņemtās</li></a>
            <a href="pm/sent/"><li class="sentmail">Sūtītās</li></a>
        </ul>
    </nav>

<table border="1" cellspacing="25" cellpadding="5" class="messagetable">
        <tr>
            <th></th>
            <th>Temats</th>
            <th>No kā?</th>
            <th>Kad?</th>
        </tr>
        
        
<?php
    $handler = new dbhandler();
    $result = $handler->query("SELECT * FROM messages WHERE recipient='{$_SESSION['userid']}' ORDER BY id DESC");
    while($row = mysqli_fetch_assoc($result)){
        $title = $row['title'];
        $title = "<a href=pm/read/".$row['id'].">".$title."</a>";
        $from = $handler->displayUsername($row['sender']);
        $date = $row['date'];
        $tr = ($row['isread'] == 0) ? "<tr class='unread'>" : "<tr class='read'>";
?>

        <?php echo $tr ?>
            <td class="image"></td>
            <td class="messagetitle"><?php echo $title ?></td>
            <td><?php echo $from ?></td>
            <td><?php echo $date ?></td>
        </tr>
<?php } ?>
    </table>
</section>