<?php
require_once('config.php');
function __autoload($class_name){
    require_once($class_name.'.php');
}
//activity updater (here, because this gets called once on every pageload)
if(isset($_SESSION['userid'])){
    $db = new db();
    $db->logUserActivity($_SESSION['userid']);
}
