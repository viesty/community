<?php
require_once 'includes/autoload.php';
require_once 'head.php';
$db = new db();
$result = $db->fetchOne("SELECT * FROM stats");

?>
<section id="maincontent">
    <header class="contentheader">Lapas statistika</header>
    <article id="statsarticle">
        <p>Lietotāji kopā: <?php echo $result['users']; ?></p>
        <p>Komentāri kopā: <?php echo $result['comments']; ?></p>
        <p>Ieraksti kopā: <?php echo $result['articles']; ?></p>
        <p>Nosūtītas vēstules: <?php echo $result['messages']; ?></p>
    </article>
</section>
<?php
require_once 'footer.php'; 
?>