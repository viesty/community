<section id="maincontent">
<?php 
    $handler = new dbhandler();
    $failed = false;
    if(is_numeric($_GET['id'])){
        $result = $handler->getUserByID($_GET['id']);
    }else{
        $failed = true;
    }
    if($failed === false && isset($_SESSION['username']) && $_SESSION['userid'] !== $_GET['id'] && $handler->affected > 0){
        $title = "";
        if(isset($_GET['reply']) && isset($_SESSION['lastpm'])){
            $res = $handler->getMessageByID($_SESSION['lastpm']);  
            if(substr($res['title'], 0, 3) !== "Re:"){
                $title = "Re: ".$res['title'];
                unset($_SESSION['lastpm']);    
            }else{
                $title = $res['title'];
            }
        }
        
        if(isset($_POST['sendmessage']) && isset($_POST['messagebody'])){
            if(!isset($_POST['messagetitle']) || strlen(strip_tags($_POST['messagetitle'])) < 1){
                $title = "[Bez nosaukuma]";
            }else{
                $title = strip_tags($_POST['messagetitle']);
            }
            $handler->sendMessage($_GET['id'], $_POST['messagebody'], $title);
        }
        
?>
    <header class="contentheader">Sūtīt vēstuli lietotājam <?php echo $result['username']; ?></header>
    <?php
        if(isset($_SESSION['incorrectCredentials'])){
            echo $_SESSION['incorrectCredentials'];
            unset($_SESSION['incorrectCredentials']);
        }
    ?>
    <form action="" method="post">
        <label for="messagetitle">Temats:</label><br />
        <input type="text" name="messagetitle" value="<?php echo $title.""; ?>" id="messagetitle"/>
        <p>Teksts:</p>
        <textarea id="messagebody" name="messagebody" class="addarticleTextArea">
            
        </textarea>      
    
        <input type="submit" name="sendmessage" value="Sūtīt" class="btn btn-success"/>
    </form>
    
<?php 
    }else{
        echo "<div class='warningBox'><p>Vai nu tu neesi ielogojies, vai nu centies nosūtīt vēstuli pats sev, vai arī ievadītais lietotāja ID ir kļūdains!</p></div>";
    }
?>
</section>

