<section id="maincontent">
    
    <?php
        $handler = new dbhandler();
        $result = $handler->getMessageByID($_GET['id']);
        //check whether the user is either recipient OR the sender of the message
        if($result['recipient'] == $_SESSION['userid'] || $result['sender'] == $_SESSION['userid']){
            $title = $result['title'];
            $text = $result['text'];
            $senderid = $result['sender'];
            $date = $result['date'];
            //default classes for menu buttons
            $navSentHighlight = "class='sentmail'";
            $navReceivedHighlight = "class='receivedmail'";
            //if the user is the sender, apply class to the upper menu 'sent' button for highlighting
            //also apply class for floating avatar and message info to the right side
            //also select recipient id for avatar
            if($senderid == $_SESSION['userid']){
                $username = $handler->displayUsername($result['recipient']);
                $class = "class='right'";
                $navSentHighlight = "class='sentmail pm_activemenu'";
                $avatarId = $result['recipient'];
            }else{
                //if the user is the recipient, apply class to the upper menu 'received' button for highlighting
                //also apply class for floating avatar and message info to the left side
                //also mark message as read on the database
                //also select sender id for avatar
                $username = $handler->displayUsername($senderid);    
                $class = "class='left'";
                $navReceivedHighlight = "class='receivedmail pm_activemenu'";
                $handler->query("UPDATE messages SET isread=1 WHERE id='{$result['id']}'");
                $_SESSION['lastpm'] = $result['id'];
                $avatarId = $senderid;
            }
    ?>
    
    <nav class="pmnav" class="clearfix">
        <ul>
            <a href="pm/received/"><li <?php echo $navReceivedHighlight; //apply classes to menu buttons ?>>Saņemtās</li></a>
            <a href="pm/sent/"><li <?php echo $navSentHighlight; ?>>Sūtītās</li></a>
        </ul>
    </nav>
    
    <div id="messagebox">
        <div id="messagecontent" class="clearfix">
            <h1><?php echo $title; ?></h1>
            <div <?php echo $class; ?>>
                <?php $handler->displayAvatar($avatarId); ?>
            </div>
            <span class="messagetext"><?php echo $text; ?></span>
        </div>
        <div id="messagebottom" class="clearfix">
            <div id="messageinfo">
                <span <?php echo $class; ?>><?php echo $username." @ ".$date; ?></span>
                <?php if($class=="class='left'"){?><a class="reply btn btn-primary" href=<?php echo "pm/reply/".$senderid; ?>>Atbildēt</a>
                <?php } //only display reply button if elements are floated to the left (thus the user is the receiver not sender)?>
            </div>
        </div>
    </div>
    <?php }else{ echo "<div class='warningBox'><p>Šī nav tava ziņa!</p></div>"; }?>
</section>