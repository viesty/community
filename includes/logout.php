<?php
require_once 'autoload.php';
$handler = new dbhandler();
if(isset($_GET['check']) && $_GET['check'] === $_SESSION['checksum']){
    //subtract 5 minutes from last_activity, so the user appears offline
    $time = new DateTime(date("Y-m-d H:i:s"));
    $time->sub(new DateInterval("PT5M"));
    $handler->query("UPDATE users SET last_activity='{$time->format('Y-m-d H:i:s')}' WHERE id={$_SESSION['userid']}");
    session_destroy();
}
header("Location: index.php");