<?php
    $handler = new dbhandler();
    if(isset($_SESSION['username']) && isset($_GET['edit']) && $_GET['edit'] == $_SESSION['userid']){
        require_once 'profiledit.php';
    }else{
        if(!isset($_GET['id']) && isset($_SESSION['userid'])){
            $id = $_SESSION['userid'];
        }elseif (!isset($_GET['id']) && !isset($_SESSION['userid'])) {
            $id = -1;
        }else{
            $id = $_GET['id'];
            $id = $handler->sanitize($id);
        }
        $userinfo = $handler->getUserByID($id);
        if($handler->affected == 1){
            //get last seen difference. 
            //If last seen over an hour ago, display date, if less then display text
            $lastSeenObject = $handler->userLastSeen($id);
            $y = $lastSeenObject->y;
            $m = $lastSeenObject->m;
            $d = $lastSeenObject->d;
            $h = $lastSeenObject->h;
            $i = $lastSeenObject->i;
            $s = $lastSeenObject->s;
            if($y < 1 && $m < 1 && $d < 1 && $h < 1){
                if($i === 0){
                    if($s < 5){
                        $lastSeen = "tikko";
                    }else{
                        $lastSeen = "pirms ".$s." sekundēm";   
                    }
                }else{
                    $lastSeen = "pirms ".$i." minūtēm";
                }
            }else{
                $lastSeen = $userinfo['last_activity'];
            }
            $username = $userinfo['username'];
            $posts = $userinfo['posts'];
            $regdate = $userinfo['regdate'];
            switch ($userinfo['usergroup']) {
                case 0:
                    $usergroup = "Lietotājs";
                    break;
                case 1:
                    $usergroup = "Administrators";
                    break;
                case 2:
                    $usergroup = "Moderators";
                    break;
                default:
                    $usergroup = "Lietotājs";
                    break;
            }
            $homepage = $userinfo['homepage'];
            $skype = $userinfo['skype'];
            $aboutme = $userinfo['aboutme'];
            if(substr($homepage, 0, 7) != "http://" && strlen($homepage) > 0){
                $homepage = "http://".$homepage;
            }
?>
<section id="maincontent">
    <div id="profilepage" class="clearfix">
        <?php if(isset($_SESSION['username']) && $id == $_SESSION['userid']){ ?>
        <header class="contentheader">Tavs profils</header>
        <?php }else{?>
        <header class="contentheader">Lietotāja <?php echo $username ?> profils</header>
        <?php } ?>
        <?php $handler->displayAvatar($id); ?>
        <div id="userinfo">
            <?php if(isset($_SESSION['username']) && $_SESSION['userid'] !== $id){ ?><p><a class="btn btn-success" href="<?php echo "pm/write/".$id ?>">Sūtīt vēstuli</a></p>
             <?php }elseif(isset($_SESSION['username']) && $_SESSION['userid'] === $id){ ?>
            <p><a class="btn btn-primary" href=<?php echo "profile/edit/".$_SESSION['userid'] ?>>Labot profilu</a></p>
            <?php
            if(isset($_SESSION['avatarError'])){
                echo $_SESSION['avatarError'];
                unset($_SESSION['avatarError']);
            }
            ?>
            <?php } ?>
            <p>Komentāri: <?php echo $posts ?></p>
            <p>Reģistrējies kopš: <?php echo $regdate ?></p>
            <p>Pēdējo reizi redzēts: <?php echo $lastSeen ?></p>
            <p>Lietotāja grupa: <?php echo $usergroup ?></p>
            <p>Mājaslapa: <a href=<?php echo $homepage ?>><?php echo $homepage ?></a></p>
            <p>Skype: <?php echo $skype ?></p>
            <p><a class="addedarticles" href="articles/<?php echo $id ?>">Lietotāja ievietotie raksti</a></p>
        </div>
        <div id="aboutme">
            <h1>Par sevi saka:</h1>
            <?php echo $aboutme ?>
        </div>
        <?php
        }else{
            echo "<div class='warningBox'><p>Profils netika atrasts vai arī tu neesi ielogojies!</p></div>";
        }?>
    </div>  
</section>
<?php
}
?>