<section id="maincontent">
    <header class="contentheader">Lietotāju saraksts</header>
    <div class="userlist">
        <p>
<?php
    $handler = new dbhandler();
    //order by admins first, mods second, users last
    $result = $handler->query("SELECT username,id FROM users ORDER BY find_in_set(usergroup, '1,2,0')"); 
    while($row = mysqli_fetch_assoc($result)){
        echo $handler->displayUsername($row['id'])." "; 
    }  
?>  
        </p>
    </div>
</section>