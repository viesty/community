<?php
if(!isset($_SESSION)){
    session_start();
}

//database credentials
defined("DB_HOST") || define("DB_HOST", 'localhost');
defined("DB_USER") || define("DB_USER", 'root');
defined("DB_PASS") || define("DB_PASS", 'viesturs123');
defined("DB_DB") || define("DB_DB", 'community');
//replace 1 with main user ID. This is the user ID from which messages will be sent to new users
defined("ADMIN_ID") || define("ADMIN_ID", '1');

//site domain name with http://
defined("SITE_URL") || define("SITE_URL", "http://".$_SERVER['SERVER_NAME']);
//directory separator C:\ on windows, linux /
defined("DS") || define("DS", DIRECTORY_SEPARATOR);
//root path
defined("ROOT_PATH") || define("ROOT_PATH", realpath(dirname(__FILE__).DS."..".DS));
//classes folder
defined("CLASSES_DIR") || define("CLASSES_DIR", "classes");
//includes folder
defined("INC_DIR") || define("INC_DIR", "includes");
//avatars folder
defined("AVAT_DIR") || define("AVAT_DIR", "avatars");
defined("HTMLP_DIR") || define("HTMLP_DIR", "htmlpurifier-4.6.0/library");

//add all above directories to the include path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(ROOT_PATH.DS.CLASSES_DIR),
    realpath(ROOT_PATH.DS.INC_DIR),
    realpath(ROOT_PATH.DS.AVAT_DIR),
    realpath(ROOT_PATH.DS.HTMLP_DIR),
    get_include_path()
)));

//developer IP's for error reporting
$devs = ['127.0.0.1', '::1'];
if(in_array($_SERVER['REMOTE_ADDR'], $devs)){
    error_reporting(E_ALL);
}else{
    error_reporting(0);
}
