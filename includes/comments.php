<?php
/*
 *  comment section populator. First of all checks whether there are any comments
 *  and reacts correspondingly. Then populates the comment div classes
 */
    $db = new db();
    $articleid = $_GET['id'];
    $result = $db->query("SELECT author,text,date,id FROM comments WHERE articleid='$articleid' AND hidden='0'");
    if($db->affected > 0){
        //add either comment count or announcement that there are no comments (in the .commentheader)
?>
<header class="contentheader">Komentāri [<?php echo $db->affected ?>]</header>
<?php }else{ ?>
<header class="contentheader">Vēl nav komentāru :( Pievienosi savu?</header>
<?php } ?>
<div id="commentbox">
<?php
    $handler = new dbhandler();
    while($row = mysqli_fetch_assoc($result)){ 
        $author = $row['author'];
        $postsResult = $db->fetchOne("SELECT posts,id FROM users WHERE username='$author'");
        $posts = $postsResult['posts'];
        $username = $handler->displayUsername($postsResult['id']);
        $date = $row['date'];
        $text = $row['text'];
        $id = $row['id'];
        //check if this is the comment's author or NOT a simple user. Show/hide edit button accordingly
        $edit = ($_SESSION['usergroup'] > 0 || $_SESSION['username'] == $author ? "<a class='editbutton btn btn-success btn-xs' href=edit.php?id=".$id.">Labot</a>" : "");
        $delete = ($_SESSION['usergroup'] > 0 ? "<a class='deletebutton btn btn-warning btn-xs' href='article.php?deleteComment=".$id."'>Dzēst</a>" : "");
        $_SESSION['lastArticle'] = $articleid;
        //get the comment author's group, according username color
?>
    
        <div class="comment clearfix">
            <div class="avatar">
                <?php $handler->displayAvatar($postsResult['id']); ?>
                <p>Komentāri: <?php echo $posts ?></p>
            </div>
            <div class="commentcontent">
                <header><?php echo $username ?> @ <?php echo $date; echo $edit; echo $delete; ?></header>
                <section><?php echo $text; ?></section>
            </div>
        </div>
        <?php } ?>
    </div>