<?php
    /*
     *  Recent event populator. 
     *  Event types:
     *  1 - New user (link to profile)
     *  2 - New article (link to article)
     *  3 - New comment (link to article)
     */
    $eventhandler = new dbhandler();
    $eventresult = $eventhandler->query("SELECT * FROM recentevents ORDER BY id DESC LIMIT 5");
    while($row = mysqli_fetch_assoc($eventresult)){
        $username = $eventhandler->displayUsername($row['eventauthor']);
        //specify the date. If event happened more  than an hour ago, show full date
        $dateNow = date("Y-m-d H:i:s");
        $dateNow = date_create($dateNow);
        $dateAdded = date_create($row['date']);
        $diff = date_diff($dateAdded, $dateNow);
        $y = $diff->y;
        $m = $diff->m;
        $d = $diff->d;
        $h = $diff->h;
        $i = $diff->i;
        $s = $diff->s;
        if($y < 1 && $m < 1 && $d < 1 && $h < 1){
            if($i === 0){
                if($s < 5){
                    $date = "Tikko";
                }else{
                    $date = "Pirms ".$s." sekundēm";   
                }
            }else{
                $date = "Pirms ".$i." minūtēm";
            }
        }else{
            $date = "@ ".$row['date'];
        }
        switch ($row['eventtype']) {
            case 1:
                $text = "Reģistrējās lapā. Sveicināts!";
                $text = "<a href=profile/".$row['eventauthor'].">".$text."</a>";
                break;
            case 2:
                $articleinfo = $eventhandler->pullArticleByID($row['articleid']);
                $text = "Izveidoja jaunu rakstu - <span class='articleemphasis'>".$articleinfo['heading']."</span>";
                $text = "<a href=article/".$row['articleid'].">".$text."</a>";
                break;
            case 3:
                $articleinfo = $eventhandler->pullArticleByID($row['articleid']);
                $text = "Pievienoja jaunu komentāru rakstā <span class='articleemphasis'>".$articleinfo['heading']."</span>";
                $text = "<a href=article/".$row['articleid'].">".$text."</a>";
                break;
            default:
                $text = "";
                break;
        }
?>
<div class="activity">
    <?php echo $username; ?>
    <p class="eventdate"><?php echo $date; ?></p>
    <p class="eventtype"><?php echo $text; ?></p>
</div>
<?php } ?>