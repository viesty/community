<?php
    /*
     * If edit parameter is set, then show the form with content from the editable article
     */
    if(isset($_GET['edit']) && is_numeric($_GET['edit'])){ //check for edit parameter
        $handler = new dbhandler();
        $result = $handler->pullArticleByID($_GET['edit']);
        if($result['author'] == $_SESSION['username'] || $_SESSION['usergroup'] > 0){
            //only show the edit form if the user is either the author or NOT a simple user
            $_SESSION['articleIdToEdit'] = $_GET['edit'];
            $heading = $result['heading'];
            $text = $result['text'];
?>
<section id="maincontent">
<?php
if(isset($_SESSION['incorrectCredentials'])){
    echo $_SESSION['incorrectCredentials'];
    unset($_SESSION['incorrectCredentials']);
}
?>
    <form action="" method="POST">
        Nosaukums:
        <input type="text" maxlength="30" name="postheading" class="postheading" value=<?php echo $heading; ?>>
        <br>
        Teksts:
        <textarea name="postcontent" class="addarticleTextArea"><?php echo $text ?></textarea>
        <br>
        <input type="submit" value="Labot" name="editarticle" class="btn btn-success"/>
    </form>
</section>

<?php 
    }else
        echo "Šis nav tavs raksts!"; //not your article or you are a simple user
}else{ //if no edit parameter, simply show the submit form
$heading = "";
$text = "";
//if adding was unsuccessful, we have saved whatever was there. Show saved text
if(isset($_SESSION['savedHeading'])){
    $heading = $_SESSION['savedHeading'];
    unset($_SESSION['savedHeading']);
}
if(isset($_SESSION['savedText'])){
    $text = $_SESSION['savedText'];
    unset($_SESSION['savedText']);
}

?>

<section id="maincontent">
<?php
if(isset($_SESSION['incorrectCredentials'])){
    echo $_SESSION['incorrectCredentials'];
    unset($_SESSION['incorrectCredentials']);
}
?>
    <form action="" method="POST">
        Nosaukums:
        <input type="text" maxlength="30" name="postheading" class="postheading" value="<?php echo $heading; ?>"/>
        <br>
        Teksts:
        <textarea name="postcontent" class="addarticleTextArea"><?php echo $text; ?></textarea>
        <br>
        <input type="submit" value="Ievietot" name="submitarticle" class="btn btn-primary"/>
    </form>
</section>
    
<?php } ?>
