<script src="scripts/jquery-2.1.1.min.js" type="text/javascript" charset="utf-8"></script>
<script src="scripts/registration.js" type="text/javascript" charset="utf-8"></script>
<?php
require_once 'includes/autoload.php';

$maxFileSize = 1000000;

if(!isset($_SESSION['username'])){ //check whether user has not already logged in
    if(isset($_POST['button_register']) && isset($_POST['f_username_register']) && isset($_POST['f_password_register']) && isset($_POST['f_email_register'])
    && isset($_POST['f_password_register_repeat']) && isset($_POST['f_bot_confirmation'])){    
        $handler = new dbhandler($_POST['f_username_register'], $_POST['f_password_register'], $_POST['f_email_register'],
        $_POST['f_password_register_repeat'], $_POST['f_bot_confirmation']);
        $handler->registration();
    }
    ?>
    
    <?php require_once 'head.php'; ?>
    <section id="maincontent">
    
    <div id="registrationForm">
        <form id="registerform" role="form" action="" method="post" enctype="multipart/form-data">
            <legend>Reģistrācija</legend>
            <div class="form-group">
               <label for="niks">Lietotājvārds</label>
               <input class="form-control" type="text" name="f_username_register" id="niks" placeholder="Ievadi lietotājvārdu"/>
               <p class="registrationIncorrectInput incUsername">Lietotājvārdam ir jābūt vismaz 3 simbolus garam!</p>
               <p class="registrationIncorrectInput usernameTaken">Izskatās, ka šāds lietotājvārds jau ir aizņemts :(</p>
               <p class="registrationIncorrectInput usernameAvailable">Lietotājvārds nav aizņemts, droši, izmanto!</p>
            </div>
            <div class="form-group">
               <label for="parole">Parole</label>
               <input class="form-control" type="password" name="f_password_register" id="parole" placeholder="Ievadi savu paroli"/>
                <p class="registrationIncorrectInput incPassword">Parolei ir jābūt vismaz 8 simbolus garai!</p>
            </div>
            <div class="form-group">
               <label for="parole-r">Parole (atkārtoti)</label>
               <input class="form-control" type="password" name="f_password_register_repeat" id="parole-r" placeholder="Pārliecināsimies, ka ir vienādas"/>
                <p class="registrationIncorrectInput incRepeat">Ievadītās paroles nesakrīt!</p>
            </div>
            <div class="form-group">
               <label for="epasts">E-Pasts</label>
               <input class="form-control" type="text" name="f_email_register" id="epasts" placeholder="piemēra@pasts.lv"/>
                <p class="registrationIncorrectInput incEmail">Ievadītais e-pasts nav pareizā formātā!</p>
            </div>
            <div class="form-group">
               <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $maxFileSize ?>" id="MAX_FILE_SIZE"/>
               <label for="file_input">Avatars (nav obligāti)</label>
               <input  type="file" name="file_avatar" id="file_input" />
               <p class="help-block">Maksimālais attēla lielums: 1 MB</p>
            </div>
            <div class="form-group">
               <label for="math">Cik ir 3 plus 5?</label>
               <input class="form-control" type="text" name="f_bot_confirmation" id="math" placeholder="Pierādi, ka neesi ļauns spambots!"/>
                <p class="registrationIncorrectInput incConfirm">Hmmm, izskaties pēc bota</p>
            </div>
            <input name="button_register" type="submit" value="Reģistrēties" class="btn btn-success"/>
        </form>
        <?php
        if(isset($_SESSION['incorrectCredentials'])){
            echo $_SESSION['incorrectCredentials'];
            unset($_SESSION['incorrectCredentials']);
        } 
        ?>
    </div>
    </section>
    <?php 
    require_once 'footer.php';
}else{
    header("Location: index.php"); //endif - logged in -> redirect to index.php
}
?>