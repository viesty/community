<?php
require_once 'includes/autoload.php';
if(isset($_SESSION['username'])){ //show the page only if logged in
    $handler = new dbhandler();
if(isset($_POST['postheading']) && isset($_POST['postcontent']) && isset($_POST['submitarticle'])){ //check for submitarticle button press
    $handler->addArticle($_POST['postheading'], $_POST['postcontent']);
}else if(isset($_POST['postheading']) && isset($_POST['postcontent']) && isset($_POST['editarticle'])){ //check for editarticle button press
    $handler->editArticle($_POST['postheading'], $_POST['postcontent'], $_SESSION['articleIdToEdit']);
}

?>

<?php require_once 'head.php'; ?>
<?php require_once 'postcontent.php';?>
<?php require_once 'footer.php'; 
}else{
    header("Location: index.php");
}?>