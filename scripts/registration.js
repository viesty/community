$(document).ready(inputHandler);
var username = false, password = false, passwordRepeat = false, 
email = false, confirm = false;
var checkTimer, doneTypingInterval = 500;


function inputHandler(){
	$('.registrationIncorrectInput').hide();
	$('input[type="submit"]').attr('disabled','disabled');
	//check if username length > 2
	$('input[name="f_username_register"]').keydown(function() {
		clearTimeout(checkTimer);
	});
	$('input[name="f_username_register"]').keyup(function() {		
		//TODO: implement taken username checking with ajax
		if($('input[name="f_username_register"]').val().length > 2){
			clearTimeout(checkTimer);
			checkTimer = setTimeout(checkUsername, doneTypingInterval);
			username = true;
			hideElement(".incUsername");
		}else{
			hideElement(".usernameTaken");
			hideElement(".usernameAvailable");
			username = false;
			showElement(".incUsername");
		}
		checkButton();
	});
	
	$('input[name="f_username_register"]').focusout(function() {
		if($('input[name="f_username_register"]').val().length > 2){
			clearTimeout(checkTimer);
			checkTimer = setTimeout(checkUsername, doneTypingInterval);
		}
	});
	
	//check if password length > 7 and both passwords match
	$('input[name="f_password_register"]').keyup(function() {
		if($('input[name="f_password_register"]').val().length > 7){
			if($('input[name="f_password_register_repeat"]').val() == $('input[name="f_password_register"]').val()){
				passwordRepeat = true;
				hideElement(".incRepeat");
			}else{
				showElement(".incRepeat");
				passwordRepeat = false;
			}
			checkButton();
			password = true;
			hideElement(".incPassword");
		}else{
			password = false;
			showElement(".incPassword");
		}
		checkButton();
	});
	$('input[name="f_password_register_repeat"]').keyup(function() {
		if($('input[name="f_password_register_repeat"]').val() == $('input[name="f_password_register"]').val()){
				passwordRepeat = true;
				hideElement(".incRepeat");
			}else{
				showElement(".incRepeat");
				passwordRepeat = false;
		}
		checkButton();
	});
	
	//check email
	$('input[name="f_email_register"]').keyup(function() {
		if(isEmail($('input[name="f_email_register"]').val()) == true){
			email = true;
			hideElement(".incEmail");
		}else{
			email = false;
			showElement(".incEmail");
		}
		checkButton();
	});
	
	//check bot
	$('input[name="f_bot_confirmation"]').keyup(function() {
		if($('input[name="f_bot_confirmation"]').val()=="8" || $('input[name="f_bot_confirmation"]').val()=="astoni"
		|| $('input[name="f_bot_confirmation"]').val()=="astoņi" || $('input[name="f_bot_confirmation"]').val()=="astonji"){
			confirm = true;
			hideElement(".incConfirm");
		}else{
			confirm = false;
			showElement(".incConfirm");
		}
		checkButton();
	});
}

function checkUsername(){
	var dataString = 'username='+$('input[name="f_username_register"]').val();
            $.ajax({
            type: "POST",
            url: "includes/check_username.php",
            data: dataString,
           	success: function(data) {
            if(data.status == 'taken'){
            	username = false;
                //username already taken
                showElement(".usernameTaken");
                hideElement(".usernameAvailable");
				checkButton();
			}else if(data.status == 'free'){
				//username is free
				username = true;
				showElement(".usernameAvailable");
				hideElement(".usernameTaken");
				checkButton();
			}
		}
	});
}

function checkButton(){
	if(username == true && password == true && email == true
		&& confirm == true && passwordRepeat == true){
			$('input[type="submit"]').removeAttr('disabled');
		}else{
			$('input[type="submit"]').attr('disabled','disabled');
	}
}
function showElement(element){
	$(element).show();
}
function hideElement(element){
	$(element).hide();
}
function isEmail(email) {
  	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  	return regex.test(email);
}