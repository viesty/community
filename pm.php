<?php
require_once 'includes/autoload.php';
?>

<?php require_once 'head.php'; ?>
<?php //figure which page to show based on GET ?page=...
    if(isset($_SESSION['username'])){
        if(!isset($_GET['page'])){
            require_once 'pm_received.php';    
            }elseif($_GET['page'] == 'sent') {
                require_once 'pm_sent.php';
            }elseif($_GET['page'] == 'write'){
                require_once 'pm_write.php';
            }elseif($_GET['page'] == 'read'){
                require_once 'pm_read.php';
            }else{
                require_once 'pm_received.php';
            }
    }else{
        echo "Jāielogojas, lai piekļūtu šai sadaļai!";
    }
?>
<?php require_once 'footer.php'; ?>