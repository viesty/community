<?php
require_once 'includes/autoload.php';

require_once 'head.php';

if(isset($_GET['viewpage'])){
    if($_GET['viewpage'] == "article"){ 
        require_once 'article.php';
    }elseif ($_GET['viewpage'] == "edit"){
    	require_once 'edit.php';
    }elseif ($_GET['viewpage'] == "pm"){
        require_once 'pm.php';
    }elseif ($_GET['viewpage'] == "post"){
        require_once 'post.php';
    }elseif ($_GET['viewpage'] == "profile"){
        require_once 'profile.php';
    }elseif ($_GET['viewpage'] == "register"){
        require_once 'register.php';
    }elseif ($_GET['viewpage'] == "users"){
        require_once 'users.php';
    }elseif ($_GET['viewpage'] == "logout"){
        require_once 'logout.php';
    }elseif ($_GET['viewpage'] == "articles"){
        require_once 'articles.php';
    }
    elseif ($_GET['viewpage'] == "stats"){
        require_once 'stats.php';
    }
    else{
        require_once 'maincontent.php'; 
    }
}else{
    require_once 'maincontent.php'; 
}
require_once 'footer.php'; 
?>