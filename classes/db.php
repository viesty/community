<?php

/**
 * Main DB class. Database credentials taken from config.php
 */
class db{
    public $affected = 0;
    public $lastQuery = 'no last query';
    protected $_connection;
	
	function __construct() {
	   $this->connect();
	}
    
    function connect(){
       $this->_connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_DB);
       if(!$this->_connection){
           die(print_r(mysqli_connect_error()));
       }
       mysqli_set_charset($this->_connection, "utf-8");
    }
    
    function logUserActivity($id){
        $id = $this->sanitize($id);
        $date = date("Y-m-d H:i:s");
        $this->query("UPDATE users SET last_activity = '$date' WHERE id='$id'");
    }
    
    function sanitize($string){ //method for escaping query input
        $string = stripslashes($string);
        require_once 'HTMLPurifier.auto.php';
        $config = HTMLPurifier_Config::createDefault();
        $config->set('HTML.Allowed', 'p,a[href|rel|target|title],img[src],span[style],strong,em,ul,ol,li');
        $purifier = new HTMLPurifier($config);
        $string = $purifier->purify($string);
        $string = mysqli_real_escape_string($this->_connection, $string);  
        return $string;
    }
    
    function query($query){
        $this->lastQuery = $query;
        //echo $this->lastQuery."<br>";
        if(!$result = mysqli_query($this->_connection, $query)){
            mysqli_error($this->_connection);
        }else{
            $this->affected = mysqli_affected_rows($this->_connection);
        }
        return $result;
    }
    
    function fetchOne($query){
        $result = $this->query($query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }
    
    
    function close(){
        if(!mysqli_close($this->_connection)){
            die("Error closing the connection");
        }
    }
    
}
