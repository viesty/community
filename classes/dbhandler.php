<?php
/**
 * DB handler with functions for register, login, posting etc.
 * 
 */
class dbhandler extends db {
    
    private $salt = "$2y$14\$8HZNmAbOMZ2ld1iTtV./aq";
    private $_password = "";
    private $_passwordRepeat = "";
    private $_username = "";
    private $_email = "";
    private $_botConfirm = "";
    public $_ip;
    
	function __construct($username="", $password="", $email="", $passwordRepeat="", $botConfirm="") {
	    parent::__construct();
		$this->_username = $username;
        $this->_password = $password;
        $this->_passwordRepeat = $passwordRepeat;
        $this->_email = $email;
        $this->_botConfirm = $botConfirm;
        $this->_ip = $_SERVER['REMOTE_ADDR'];
        ob_start();
	}
    
    function registration(){
        //sanitize all input
        $this->_username = $this->sanitize($this->_username);
        $this->_username = strip_tags($this->_username);
        $this->_password = $this->sanitize($this->_password);
        $this->_passwordRepeat = $this->sanitize($this->_passwordRepeat);
        $this->_botConfirm = $this->sanitize($this->_botConfirm);
        $this->_email = $this->sanitize($this->_email);
        $allowedConfirmations = ["8", "astoņi", "astonji", "astoni"];
        //check whether the fields are of valid values
        
        if(!filter_var($this->_email, FILTER_VALIDATE_EMAIL)
            || strlen($this->_username)<3 
            || strlen($this->_password)<8
            || $this->_password !== $this->_passwordRepeat
            || !in_array($this->_botConfirm, $allowedConfirmations)){
            //display a message that indicates invalid email.
            
            $_SESSION['incorrectCredentials'] = "<div class='warningBox'><p>Reģistrācija neizdevās, pārabudi ievadīto datu pareizību visos laukos!</p></div>";
            header("Location: register");
            die();
        }
        
        //avatar filename getter
        $filename = $this->processAvatar();
        
        //check for existing records of username OR email
        $this->query("SELECT * FROM users WHERE username='$this->_username' OR email='$this->_email'");
        $affected = $this->affected;
        if($affected>0){
            $_SESSION['incorrectCredentials'] = "<div class='warningBox'><p>Lietotājs ar šādu vārdu vai e-pastu jau reģistrēts!</p></div>";
            //user already exists, redirect and display the message
            header("Location: register");
            die();
        }else
        //Greeting message
        $title = "Sveicināts!";
        $text = "Skatos, ka tikko piereģistrējies. Laipni tevi lūdzu mūsu saimē un vēlu pavadīt šeit patīkamu laiku! :)";
        
        //If all of this has passed, it's pretty safe to add the user to the database
        $date = date("Y-m-d H:i:s");
        $passhash = crypt($this->_password, $this->salt);
        $query = "INSERT INTO users (username,password,email,regdate,avatar) VALUES ('$this->_username','$passhash','$this->_email', '$date', '$filename')";
        $this->query($query);
        if($this->affected > 0){
            //register successful
            $userinfo = $this->fetchOne("SELECT id FROM users WHERE username='$this->_username'");
            $query = "INSERT INTO recentevents (eventtype,eventauthor,date) VALUES (1,'{$userinfo['id']}','$date')";
            $this->query($query);
            //greeting
            $adminid = ADMIN_ID;
            $this->query("INSERT INTO messages (recipient,sender,title,text,date) VALUES ('{$userinfo['id']}','$adminid','$title','$text','$date')");
            $this->updateStats("messages");
            //increment registered user stats
            $this->updateStats("users");
            $_SESSION['registrationSuccessful'] = "<div class='warningBox successful'><p>Reģistrācija veiksmīga!</p></div>";
            header("Location: /community/");
            $this->close();
            }else{
            //register unsuccessful
            $_SESSION['incorrectCredentials'] = "<div class='warningBox'><p>Reģistrācija neizdevās, pārabudi ievadīto datu pareizību visos laukos!</p></div>";
            $this->close();
        }
        
    }
    
    function processAvatar(){
        if(isset($_FILES['file_avatar']) && @is_uploaded_file($_FILES['file_avatar']['tmp_name'])){
            if(!@getimagesize($_FILES['file_avatar'][tmp_name]) || filesize($_FILES['file_avatar'][tmp_name]) > 1000000){
                $_SESSION['avatarError'] = "<div class='warningBox'><p>Augšupielādētais attēls ir pārāk liels vai arī nemaz nav attēls!</p></div>";
                return "none.png";
            }else{
                $targetDir = "avatars/";
                require_once 'php_image_magician.php';
                $width = 100;
                $height = 100;
                $option = 3;
                //where to store uploaded image temporarily
                $tmp_file = "uploads/".time().rand(1,99999).$_FILES['file_avatar']['name']; 
                //move the uploaded file there
                move_uploaded_file($_FILES['file_avatar']['tmp_name'], $tmp_file);
                $imageLibObj = new imageLib($tmp_file);
                //filename for the ready avatar. This will go into the DB
                $filename = time()."-".rand(1, 999999).".png";
                $imageLibObj -> resizeImage($width, $height, $option=0, $sharpen=false);
                $imageLibObj -> saveImage($targetDir.$filename); 
                unlink($tmp_file); //delete the temporary uploaded image
                return $filename;
            }
        }
        return "none.png";
    }
    
    function login(){
        //sanitize the input
        $this->_username = $this->sanitize($this->_username);
        $this->_password = $this->sanitize($this->_password);
        //check if there is a match in db
        $passhash = crypt($this->_password, $this->salt);
        $query = "SELECT * FROM users WHERE username='$this->_username' AND password='$passhash'";
        $result = $this->query($query);
        if($this->affected !== 1){
            //login info is incorrect. terminate the connection, add to logs and show a message indicating so
            $this->logAction($this->_ip, "Nesekmīgi mēģināja ielogoties lietotājā", $this->_username);
            $this->close();
            $_SESSION['incorrectCredentials'] = "<div class='warningBox'><p>Lietotājvārds vai parole nav pareizi!</p></div>";
            header("Location: index.php");
            die();
        }else{
            $row = $result->fetch_assoc();
            $_SESSION['username'] = $row['username'];
            $_SESSION['userid'] = $row['id'];
            $_SESSION['usergroup'] = $row['usergroup'];
            //checksum for logout confirmation
            $_SESSION['checksum'] = substr(md5(rand(0, 1000).$_SESSION['username']."4rCHlInux"), 0, 6);
            $this->logAction($this->_ip, "Ielogojās profilā", $_SESSION['userid']);
            header("Location: ". $_SERVER['HTTP_REFERER']);
            $this->close();
            die();
        }
        //set session cookie username and id
        //redirect to page the user is in
    }

    function addArticle($heading, $text){
        //sanitize input
        $heading = $this->sanitize($heading);
        $text = $this->sanitize($text);
        $heading = strip_tags($heading);
        //add the article only if session exists
        //check the actual length of the text
        $tmpText = strip_tags($text);
        if(isset($_SESSION['username'])){
            if(strlen($heading) > 2 && strlen($tmpText) > 2){
                $date = date("Y-m-d H:i:s");
                $query = "INSERT INTO articles (author,heading,text,date,authorid) VALUES ('{$_SESSION['username']}','$heading','$text','$date','{$_SESSION['userid']}')";
                $this->query($query);
                $insertedid = mysqli_insert_id($this->_connection);
                if($this->affected > 0){
                    //article insert success
                    $this->updateStats("articles");
                    $articleinfo = $this->fetchOne("SELECT id FROM articles WHERE authorid='{$_SESSION['userid']}' AND date='$date'");
                    $query = "INSERT INTO recentevents (eventtype,articleid,eventauthor,date) VALUES (2,'{$articleinfo['id']}','{$_SESSION['userid']}','$date')";
                    $this->query($query);
                    header("Location: article/".$insertedid);
                }else{
                    //article adding unsuccessful
                    $_SESSION['incorrectCredentials'] = "<div class='warningBox'><p>Kļūda raksta pievienošanā, pārbaudi visus laukus!</p></div>";
                    $_SESSION['savedHeading'] = $heading;
                    $_SESSION['savedText'] = $text;
                }
            }else{
                //either heading or text is shorter than 2 chars
                $_SESSION['incorrectCredentials'] = "<div class='warningBox'><p>Tekstam un nosaukumam jābūt vismaz 3 simbolus garam!</p></div>";
                $_SESSION['savedHeading'] = $heading;
                $_SESSION['savedText'] = $text;
            }
        }else{
            //user is not logged in, gtfo
            header("Location: index.php");
            $this->close();
            die();
        }
    }
    
    function editArticle($heading, $content, $id){
        if(isset($_SESSION['username']) && strlen($heading) > 2 && strlen($content) > 2){
            $heading = $this->sanitize($heading);
            $content = $this->sanitize($content);
            $id = $this->sanitize($id);
            $this->query("UPDATE articles SET heading='$heading', text='$content' WHERE id='$id'");
            $this->logAction($_SESSION['userid'], "Laboja rakstu", $id);
            //go to the edited article
            header("Location: article/".$_SESSION['articleIdToEdit']);
            unset($_SESSION['articleIdToEdit']);
        }else{
            header("Location: index.php");
            $this->close();
        }    
    }
    
    function addComment($text, $id){
        if(isset($_SESSION['username'])){
            $text = $this->sanitize($text);
            $id = $this->sanitize($id);
            //real length of comment
            $tempText = strip_tags($text);
            if(strlen($tempText)>0){
                $id = $this->sanitize($id);
                $date = date("Y-m-d H:i:s");
                $author = $_SESSION['username'];
                $authorid = $_SESSION['userid'];
                $this->query("INSERT INTO comments (articleid, author, text, date, authorid) VALUES ('$id','$author','$text','$date', '$authorid')");
                if($this->affected > 0){
                    //comment add successful
                    $this->updateStats("comments");
                    //increase user post count by 1
                    $this->query("UPDATE users SET posts = posts + 1 WHERE username='$author'");
                    //increase article commentcount by 1
                    $this->query("UPDATE articles SET commentcount = commentcount + 1 WHERE id='$id'");
                    $query = "INSERT INTO recentevents (eventtype,articleid,eventauthor,date) VALUES (3,'$id','$authorid','$date')";
                    $this->query($query);
                    header("Location: ".$id);
                }else{
                    //comment add unsuccessful
                    $_SESSION['incorrectCredentials'] = "<div class='warningBox'><p>Komentāra pievienošana neizdevās! Pārbaudi visu ievadi!</p></div>";
                    header("Location: index.php");
                }
            }else{
                $_SESSION['incorrectCredentials'] = "<div class='warningBox'><p>Komentārs pārāk īss!</p></div>";
                $this->close();
            }
        }
    }
    function editComment($id, $text){
        $id=$this->sanitize($id);
        $text=$this->sanitize($text);
        if(strlen(strip_tags($text))>1 && is_numeric($id)){
            $this->query("UPDATE comments SET text='$text' WHERE id='$id'");
            //get the article id by checking to which article comment is aimed
            $commentres = $this->pullCommentByID($id);
            $this->logAction($_SESSION['userid'], "Laboja komentāru", $id);
            header("Location: article/".$commentres['articleid']);
        }else{
            header("Location: index.php");
        }
    }
    function pullCommentByID($id){
        $id = $this->sanitize($id);
        $result = $this->fetchOne("SELECT * FROM comments WHERE id='$id'");
        return $result;
    }
    function pullArticleByID($id){
        $id = $this->sanitize($id);
        $result = $this->fetchOne("SELECT * FROM articles WHERE id='$id'");
        if($this->affected > 0){
            return $result;
        }else{
            return "fail";
        }
    }
    function getUser($username){
        $username = $this->sanitize($username);
        $result = $this->fetchOne("SELECT * FROM users WHERE username='$username'");
        return $result;
    }
    function getUserByID($userid){
        $userid = $this->sanitize($userid);
        $result = $this->fetchOne("SELECT * FROM users WHERE id='$userid'");
        return $result;
    }
    function getMessageByID($id){
        $id = $this->sanitize($id);
        $result = $this->fetchOne("SELECT * FROM messages WHERE id='$id'");
        return $result;
    }
    function getUserColor($username){
        //apply the corresponding username color based on the usergroup
        $authorinfo = $this->getUser($username);
        switch ($authorinfo['usergroup']) {
        case 0:
            $authorspan = "<span class='simpleuser'>$username</span>";
            break;
        case 1:
            $authorspan = "<span class='admin'>$username</span>";
            break;
        case 2:
            $authorspan = "<span class='moderator'>$username</span>";
            break;
        default:
            $authorspan = "<span class='simpleuser'>$username</span>";
            break;
        }
        return $authorspan;
    }
    function displayUsername($id){
        $id = $this->sanitize($id);
        $result = $this->fetchOne("SELECT username FROM users WHERE id='$id'");
        $result = $this->getUserColor($result['username']);
        $result = "<a href=profile/".$id.">".$result."</a>"; //prepend the <a> tag to the <span> tag
        return $result;
    }
    
    function displayAvatar($id){
        $id = $this->sanitize($id);
        $result = $this->fetchOne("SELECT avatar FROM users WHERE id='$id'");
        echo "<img src='avatars/".$result['avatar']."' />";
    }
    
    function updateProfile($id, $website, $skype, $aboutme){
        $id = $this->sanitize($id);
        $website = $this->sanitize($website);
        $skype = $this->sanitize($skype);
        $aboutme = $this->sanitize($aboutme);
        $avatarinfo = $this->getUserByID($id);
        //check if the return of processavatar function is none.png (which means that no avatar uploaded or there are errors with it)
        $av = $this->processAvatar();
        if($av !== "none.png"){
            $filename = $av;
        }else{
            //if no avatar is uploaded, save the same filename as before on the DB
            $filename = $avatarinfo['avatar'];
        }
        $this->query("UPDATE users SET homepage='$website', skype='$skype', aboutme='$aboutme', avatar='$filename' WHERE id='$id'");
        $this->logAction($_SESSION['userid'], "Laboja profilu", $id);
        header("Location: /community/profile");
    }
    
    function usergroupChange($user, $group){
        $user = $this->sanitize($user);
        $group = $this->sanitize($group);
        if(filter_var($group, FILTER_VALIDATE_INT)){
            $query="UPDATE users SET usergroup='$group' WHERE username='$user'";
        }
    }
    
    function sendMessage($id, $text, $title){
        $text = $this->sanitize($text);    
        $tmpText = strip_tags($text);
        $id = $this->sanitize($id);
        $title = $this->sanitize($title);
        if(strlen($tmpText) > 0){
            $sender = $_SESSION['userid'];
            $date = date("Y-m-d H:i:s");
            $this->updateStats("messages");
            $this->query("INSERT INTO messages (recipient,sender,title,text,date) VALUES ('$id','$sender','$title','$text','$date')");
            header("Location: /community/pm/sent");
        }else{
            $_SESSION['incorrectCredentials'] = "<div class='warningBox'><p>Izskatās, ka tu neierakstīji nekādu tekstu!</p></div>";
        }
    }
    
    function logAction($user, $action, $location){
        $user = $this->sanitize($user);
        $user = $this->sanitize($action);
        $user = $this->sanitize($location);
        $ip = $_SERVER["REMOTE_ADDR"];
        $date = date("Y-m-d H:i:s");
        $this->query("INSERT INTO actionlog (user, action, location, date, ip) VALUES ('$user', '$action', '$location', '$date', '$ip')");
    }
    
    function populateUserArticles($id){
        $id = $this->sanitize($id);
        $result = $this->getUserByID($id);
        if($this->affected < 1){
            echo "<div class='warningBox'><p>Lietotājs ar šādu ID neeksistē!</p></div>";
        }else{
            echo "<header class='contentheader'>Lietotāja ".$result['username']." izveidotie ieraksti</header>";
            $articles = $this->query("SELECT * FROM articles WHERE authorid='$id' AND hidden='0' ORDER BY id DESC");
            if($this->affected < 1){
                echo "<div class='warningBox'><p>Lietotājs ".$result['username']." nav pievienojis nevienu ierakstu!</p></div>";
            }else{
                echo "<table border='1' cellspacing='25' cellpadding='5' class='messagetable articles'>";
                while($row = mysqli_fetch_assoc($articles)){
                    echo "<tr>";
                    echo "<td class='title'><a href='article/".$row['id']."'>".$row['heading']."</a></td> 
                    <td><span class='commentcount'>[Komentāri:".$row['commentcount']."]</span></td>
                    <td><span class='date'> @ ".substr($row['date'],0, 10)."</span></td>";
                    echo "</tr>";
                }
                echo "</table>";
            }
        }
    }

    function userLastSeen($id){
        $id = $this->sanitize($id);
        $result = $this->query("SELECT last_activity FROM users WHERE id='$id'");
        $result = mysqli_fetch_assoc($result);
        $currentDate = date("Y-m-d H:i:s");
        $currentDate = date_create($currentDate);
        $lastDate = date_create($result['last_activity']);
        return date_diff($lastDate, $currentDate);
    }
    
    function updateStats($stat){
        $stat = $this->sanitize($stat);
        $this->query("UPDATE stats SET $stat = $stat + 1");
    }
}
